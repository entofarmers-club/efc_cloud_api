FROM keymetrics/pm2:8-alpine
MAINTAINER Arn van der Pluijm <https://gitlab.com/avdp>

EXPOSE 4000 1883

WORKDIR /src

COPY package*.json ./
COPY ecosystem*.config.js ./
COPY tsconfig*.json ./

RUN npm install

COPY . .

CMD npm run dev
