import { Server } from 'aedes';
import { createServer } from 'net';
var httpServer = require('http').createServer();
var ws = require('websocket-stream')
import { config } from './config';
import { MqttController } from './controllers/mqtt/mqtt.controller';

// MQTT Server application
export default class MQTT {

  mqttController: any;
  broker: any;
  server: any;
  port = config.server.mqttPort;
  wsPort = 9001;

  constructor() {
    this.mqttController = new MqttController();
  }

  initialize(): boolean {

    // Setup the broker (aedes mqtt)
    this.broker = Server({
      concurrency: 100,
      heartbeatInterval: 60000,
      connectTimeout: 30000,
      authenticate: this.mqttController.authenticate,
      authorizePublish: this.mqttController.authorizePublish,
      // authorizeSubscribe: this.mqttController.authorizeSubscribe,
      // authorizeForward: this.mqttController.authorizeForward,
      // published: this.mqttController.published
    });

    // Setup the public server
    this.server = createServer(this.broker.handle)

    this.server.listen(this.port, function () {
      console.error('server listening on port', this.port, 'pid', process.pid)
    });

    ws.createServer({
      server: httpServer
    }, this.broker.handle);

    let self=this;
    httpServer.listen(this.wsPort, function () {
      console.log('websocket server listening on port', self.wsPort)
    })

    // Setup broker handlers
    this.mqttController.createSubscriptions(this.broker);

    return true;
  }
}
