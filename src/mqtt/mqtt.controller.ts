import { getRepository } from 'typeorm';
import {
  IPublishPacket,
  // ISubscribePacket,
  // ISubscription,
  // IUnsubscribePacket
} from 'mqtt-packet'
import {
  Client,
  AuthenticateError
} from 'aedes'
import Farm from './../../controllers/farm/farm.entity';
import DatapointService from './../../controllers/datapoint/datapoint.service';
import logger from './../../utils/logger';

interface FarmClient extends Client {
  farmId?: string
}

export class MqttController {

  constructor() {}

  /**
   * Validate credentials when a new client connects
   *
   *   callback(err, succesful: boolean)
   *     Error.returncode: number
   *       1 - Unacceptable protocol version
   *       2 - Identifier rejected
   *       3 - Server unavailable
   *       4 - Bad user name or password
   **/
  async authenticate(client: FarmClient, username: string, password: string, callback: any) {

    // let err: AuthenticateError = null;
    // let valid: boolean = false;
    let farm: Farm;

    logger.info(`[ mqtt.controller ] - Authenticate new client ${client.id}, username: ${username}, password: ${password}`);

    // Validate input
    if (!client.id || !username || !password) {
      logger.debug({ message: 'No client, username and/or password' });
      let error = new Error() as AuthenticateError;
      error.returnCode = 1
      return callback(error, null);
    }

    try {
      farm = await getRepository(Farm).findOne({
        where: {
          mqtt_username: username,
          mqtt_password: password.toString()
        }
      });
    } catch(err) {
      logger.info(JSON.stringify(err));
      logger.error({err});
      let error = new Error() as AuthenticateError
      error.returnCode = 3;
      return callback(error, null);
    }

    // Check if a farm was found.
    if (!farm) {
      logger.debug('Farm not found for provided user and pass. ');
      let error = new Error() as AuthenticateError
      error.returnCode = 4;
      return callback(error, null);
    }

    client.farmId = farm.mqtt_username;
    return callback(null, true);
  }

  // Validate if a client is allowed to publish to the desired topic
  authorizePublish(client: FarmClient, packet: IPublishPacket, callback: any) {
    logger.info({ message: 'Authorizing publish', client: client.id, farmId: client.farmId, topic: packet.topic, payload: packet.payload.toString() });

    let topicFarm = packet.topic.split('/')[ 2];
    if ( client.farmId !== topicFarm ) {
      logger.info({ message: 'Publishing not allowed', client: client.id, topic: packet.topic, allowed: false });
      return callback(new Error('not allowed'))
    }

    logger.info({ message: 'Publishing is allowed', client: client.id, topic: packet.topic, allowed: true });
    callback(null);
  }


  // // validate if a client is allowed to subscribe to a topic
  // authorizeSubscribe(client: Client, sub: ISubscription, cb: any) {
  //   logger.info({ message: 'Authorizing subscribe', client: client.id, topic: sub.topic });
  //
  //   // if (client.farm_id.toString() !== sub.topic.split('/')[ 2]) {
  //     // sub = null;
  //     // logger.info({ message: 'Authorizing subscribe result', client: client.id, allowed: false });
  //   // }
  //
  //   cb(null, sub);
  // }

  // authorizeForward(client: Client, packet: IPublishPacket) {
  //   if (packet.topic === 'aaaa' && client.id === 'I should not see this') {
  //     return null
  //     // also works with return undefined
  //   } else if (packet.topic === 'aaaa' && client.id === 'I should not see this either') {
  //     return
  //   }
  //
  //   if (packet.topic === 'bbb') {
  //     packet.payload = new Buffer('overwrite packet payload')
  //   }
  //
  //   return packet;
  // }

  // Handle packet published result
  published(packet: IPublishPacket, _client: Client, done:any) {
    packet.payload = packet.payload.toString();
    logger.debug({ message: 'mqtt published', packet });
    done(null);
  }

  // Subscribe to broker events
  createSubscriptions(broker: any) {
    logger.info(`[ mqtt.controller ] - Create subscriptions`);

    broker.on('closed', () => {
      console.log(`closed`)
    });

    broker.on('client', async (client: any)  => {
      logger.info(`[ mqtt.controller ] - Client connected: ${client.id}`);
      // Update farm status
      try {
        const farm = await getRepository(Farm).findOne({ where: { mqtt_username: client.farmId }});
        if (farm) {
          farm.mqtt_status = true;
          await getRepository(Farm).save(farm);
          logger.info(`[ mqtt.controller ] - Farm status is set to online`);
        }
      } catch(err) {
        logger.error(err);
      }
      logger.info(`[ mqtt.controller ] - Farm status updated (online)`);
    });

    broker.on('clientDisconnect', async (client: any) => {
      logger.info(`[ mqtt.controller ] - Client: ${client.id} disconnected`);
      // Update farm status
      try {
        const farm = await getRepository(Farm).findOne({ where: { mqtt_username: client.farmId }});
        if (farm) {
          farm.mqtt_status = false;
          await getRepository(Farm).save(farm);
          logger.info(`[ mqtt.controller ] - Farm status is set to offline`);
        }
      } catch(err) {
        logger.error(err);
      }
      logger.info(`[ mqtt.controller ] - Farm status updated (offline)`);
    })

    // broker.on('keepaliveTimeout', (client: any) => {
    //   console.log(`client: ${client.id} timed out`)
    // })

    // broker.on('connackSent', (client: any) => {
    //   console.log(`client: ${client.id} connack sent`)
    // })

    broker.on('clientError', (client: any) => {
      console.log(`client: ${client.id} error`)
    })

    broker.on('connectionError', (_client: any) => {
      console.log('connectionError')
    })

    // broker.on('ping', (packet:any, client: any) => {
    //   // console.log(`client: ${client.id} ping with packet ${packet.cmd}`)
    // });

    // broker.on('publish', (packet:any, client:any) => {
    //   if (client) {
    //     console.log(`client: ${client.id} published packet ${packet.id}`)
    //   }
    // })

    broker.on('ack', (packet:any, client:any) => {
      console.log(packet);
      console.log(`client: ${client.id} ack with packet ${packet}`)
    })

    // broker.on('subscribe', (_subscriptions:any, client:any) => {
    //   console.log(`client: ${client.id} subscribe`)
    // })
    //
    // broker.on('unsubscribe', (_subscriptions:any, client:any) => {
    //   console.log(`client: ${client.id} subsribe`)
    // })

    broker.on('clientError', (client:any, err:any) => {
      console.error('client error', client.id, err.message)
    });

    // Handle status update of client
    broker.subscribe('status/#', async (packet:any, cb:any) => {
      const status = packet.payload.toString('UTF-8');
      const farmId = packet.topic.split('/')[ 1];
      logger.info(`[ mqtt.controller ] - Client status update to ${status} for farm ${farmId}`);

      if (status === '0') {
        try {
          const farm = await getRepository(Farm).findOne(farmId);
          farm.mqtt_status = false;
          await getRepository(Farm).save(farm);
        } catch(err) {
          logger.warn(err);
        }
      }
      cb();
    });

    // subscribe to new clients
    broker.subscribe('$SYS/+/new/clients', (packet:any, cb:any) => {
      logger.info(`[ mqtt.subscribe ] - New client ${packet.payload.toString()}`);
      // clients.push(packet.payload.toString());
      cb();
    });

    // // Listen for disconnects
    // aedes.subscribe('$SYS/+/disconnect/clients', (packet:any, cb:any) => {
    //   let val = packet.payload.toString();
    //   console.log(val);
    //   let j = 0;
    //   for (let i=0; i<clients.length; i++) {
    //     if (clients[ i] !== val) {
    //       clients[ j++] = clients[ i];
    //     }
    //   }
    //   clients.length = j;
    // });

    /**
     * Listen to incoming streamdata
     **/
    broker.subscribe('v1/farm/+/stream/+', async (packet:any, cb:any) => {

      logger.info(`[ mqtt.controller ] - Stream subscribe: New stream data. `);

      let farmId: string = packet.topic.split('/')[ 2];
      let streamId: string = packet.topic.split('/')[ 4];
      let value: string = packet.payload.toString();
      logger.info(`[ mqtt.controller ] - Stream subscribe: New datapoint received ${farmId}, ${streamId}, ${value}. `);

      await DatapointService.addDatapoint({ farmId, streamId, value });

      logger.info(`[ mqtt.controller ] - Stream subscribe: datapoint done. `);

      cb();
    });
  }
}
