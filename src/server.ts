import 'reflect-metadata';
import { bootstrap } from './bootstrap';
import App from './api';
import MQTT from './mqtt/mqtt';
import * as controllers from './controllers';
import Controller from './interfaces/controller.interface';

bootstrap().then(async _connection => {

  // initialize controllers
  let controllersArray: Controller[] = [];
  Object.keys(controllers).forEach(key => {
    controllersArray.push( new (<any>controllers)[ key]() );
  });

  // Initialize API
  const app = await new App(controllersArray);
  await app.initialize();
  app.listen();

  // MQTT Server
  const mqtt = new MQTT();
  mqtt.initialize();

}).catch(error => console.log('server error: ', error));
