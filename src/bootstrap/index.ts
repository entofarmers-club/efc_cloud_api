import {
  Connection,
  getRepository } from "typeorm";
import Datatype from './../controllers/datatype/datatype.entity';
import DatatypeUnit from './../controllers/datatype/datatype_unit.entity';
import {
  DatatypeSeed,
  DatatypeUnitSeed } from './../controllers/datatype/datatypes.seed';
import CreateDatatypeDto from './../controllers/datatype/datatype.dto';
import * as dotenv from 'dotenv';
import { databaseConnection } from './../bootstrap/database-connection';
import { config } from './../config';

let database: Connection;


// Seed datatypes from file
async function seedDatatypes() {
  try {

    // Create datatype units
    await getRepository(DatatypeUnit).save(DatatypeUnitSeed);
    console.log('datatype_units created');

    // Create datatypes
    let a: CreateDatatypeDto[] = DatatypeSeed;
    await getRepository(Datatype).save(a);
    console.log('datatypes created');

    // Create the relations between datatype and datatype unit
    for (let i=0; i<DatatypeSeed.length; i++) {
      if (DatatypeSeed[ i].units) {
        for( let k=0; k<DatatypeSeed[ i].units.length; k++) {
          let dtu = await getRepository(DatatypeUnit).findOne({ where: { constant: DatatypeSeed[ i].units[ k] } });
          let dt = await getRepository(Datatype).findOne({
            where: { constant: DatatypeSeed[ i].constant },
            relations: ['units']
          });
          dt.units.push(dtu);
          await database.manager.save(dt);
        }
      }
    }
    console.log('seed done');
  } catch(err) {
    console.log(err);
  }
}

// Bootstrap the application async
export const bootstrap = async (): Promise<Boolean> => {
  console.log('Bootstrap - Start');

  try {
    const dbSync = config.db.sync;

    // Load environment variables from .env file, where API keys and passwords are configured
    dotenv.config({ path: '.env' });
    // create connection with database
    database = await databaseConnection();
    console.log('Bootstrap - databaseConnection created');

    await database.runMigrations();
    console.log('Bootstrap - Migrations ran');
    if (dbSync) {
      await database.synchronize(true)
      console.log('Bootstrap - Synchronize done');
      await seedDatatypes();
      console.log('Bootstrap - Seeding done');
    }

  } catch (err) {
    console.log('Bootstrap - error', err);
  }

  console.log('Bootstrap - done');
  return true;
};

export const Database: Connection = database;
