import sendgrid from '@sendgrid/mail';
import { config } from './../../config';
import logger from './../../utils/logger';
import * as templates from './../../utils/email/templates';
// import User from './controllers/user/user.entity';

sendgrid.setApiKey(config.email.sendgrid.secret);

export class iEmailData {
	type: string
	to: string
	subject?: string
	toProfile?: any
	fromProfile?: any
}

// send a mail with a template
export async function send(data: iEmailData): Promise<any> {

	logger.debug({ message: `[ email.index ] - Initiate sending new email: ${data.type}`, data });

	// Validate template
	if ( !(<any>templates)[ data.type] ) {
		logger.warn('[ email.index ] - No email template found');
		return false;
	}

	// Fetch the mail template
	let td: any = (<any>templates)[ data.type](data);
	logger.debug({ message: `[ email.index ] - template data for ${data.type} found`, td });

	// Setup email message object
	let msg: any = {
		to: data.to,
		templateId: config.email.sendgrid.template,
	  from: {
			name: config.email.sender.default.name,
      email: config.email.sender.default.email
		},
		dynamic_template_data: td
	}

	return sendgrid.send(msg);

}

// Quick email send.
export async function sendSimple(msg: any): Promise<any> {

	let d: any = {
		to: msg.to,
		templateId: config.email.sendgrid.template,
		from: {
			name: config.email.sender.default.name,
			email: config.email.sender.default.email
		},
		dynamic_template_data: {
			subject: msg.subject,
			html: msg.html
		}
	};

	return await sendgrid.send(d);
}
