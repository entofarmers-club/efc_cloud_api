import bodyParser from 'body-parser';
import express from 'express';
import Controller from './interfaces/controller.interface';
import errorMiddleware from './middleware/error.middleware';
// import compression from 'compression';
import helmet from 'helmet';
import cors from 'cors';
import jwt from 'express-jwt';
import { config } from './config';
// import { default as Cron } from './cron';

export default class App {

  private app: express.Application;
  private controllers: Controller[];

  constructor(_controllers: Controller[]) {
    this.controllers = _controllers;
  }

  async initialize(): Promise<boolean> {
    this.app = express();

    this.initializeMiddlewares();
    await this.initializeControllers(this.controllers);
    this.initializeErrorHandling();

    // initialize cron
    // new Cron().init();

    return true;
  }

  public listen() {
    this.app.listen(process.env.NODE_PORT, () => {
      console.log(`App listening on the port ${process.env.NODE_PORT}`);
    });
  }

  public getServer() {
    return this.app;
  }

  private initializeMiddlewares() {

    // this.app.use(compression());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json({ limit: '5mb' }));

    // Use helmet to secure Express headers
    let SIX_MONTHS = 1000*60*60*24*31*6;
    this.app.use(helmet.frameguard());
    this.app.use(helmet.xssFilter());
    this.app.use(helmet.noSniff());
    this.app.use(helmet.ieNoOpen());
    this.app.use(helmet.hsts({maxAge: SIX_MONTHS, includeSubDomains: true, force: true}));
    this.app.use(helmet.noCache());
    this.app.disable('x-powered-by');

    this.app.use(cors({ maxAge: 1728000 }));

    // this.app.use((req,res,next) => {
    //   console.log(req.headers);
    //   next();
    // });

    // Use JWT
    let jwtCheck: any = jwt(config.auth0.jwtSettings);
    this.app.use(jwtCheck.unless({
      path: [
        { url: '/', methods: [ 'GET' ] },                 // API Root
      ]
    }));

    // initiate auth service
    require('./services/auth0.service');

  }

  private initializeErrorHandling() {
    this.app.use(errorMiddleware);
  }

  private initializeControllers(controllers: Controller[]) {
    controllers.forEach((controller) => {
      this.app.use('/', controller.router);
    });
  }
}
