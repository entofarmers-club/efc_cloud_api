import schedule from 'node-schedule';
import logger from './../utils/logger';

class DemoCron {

  public interval = '0 0 * * 1,3,5';

  constructor() {
    logger.info({ message: '[ cron: demo ] - constructor', interval: this.interval });
    schedule.scheduleJob(this.interval, this.job);
  }

  // Initiate the cron
  public async job() {
    logger.info({ message: '[ cron: demo ] - Job' });
  }
}

export default DemoCron;
