import DemoCron from './../cron/demo.cron';
import logger from './../utils/logger';

export function InitCron() {
  logger.info({ message: 'cron.index constructor' });
  new DemoCron();
}
