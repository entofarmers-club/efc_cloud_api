import * as dotenv from 'dotenv';
import jwks from 'jwks-rsa';
const version = require('./../package.json').version;

dotenv.config({ path: '.env'});

export interface IConfig {
  version: string,
	env: string,
	domain: string,
	product: {
		url: string,
		auth_url: string,
		api_url: string,
		name: string,
		author: string,
		version: string,
	},
	server: {
		url: string,
		port: number,
    mqttPort: number

	},
	auth0: {
		domain: string,
		clientId: string,
		clientSecret: string,
		scope: string,
		jwtSettings: {
			secret: any,
			audience: string,
			issuer: string,
			algorithms: [string]
		}
	},
	db: {
		host: string,
		port: number
		dialect:  string,
		username: string,
		password: string,
		database: string,
    sync: boolean
	},
	logger: {
		level: string,
		host: string,
		port: number
	},
	sentry: {
		dsn: string,
	},
	email: {
		backend: 'sendgrid',
		sender: {
			default: {
				name: string,
				email: string,
			},
			support: {
				name: string,
				email: string,
			},
		},
		sendgrid: {
			secret: string,
			template: string,
		},
	},
	recaptcha: {
    key     : string,
    secret  : string,
  },
  admin: {
    name  : string,
    email : string,
  }
}

const config: IConfig = {

	version: version,

	env: process.env.NODE_ENV,

	domain: process.env.DOMAIN,

	product: {
		url: process.env.PRODUCT_URL,
		auth_url: process.env.AUTH_URL,
		api_url: process.env.API_URL,
		name: process.env.PRODUCT_NAME,
		author: process.env.PRODUCT_AUTHOR,
		version: process.env.VERSION
	},

	server: {
		url: process.env.BASE_URL,
		port: Number(process.env.NODE_PORT),
    mqttPort: Number(process.env.MQTT_PORT),
	},

	auth0: {
		domain: process.env.AUTH0_DOMAIN,
		clientId: process.env.AUTH0_CLIENTID,
		clientSecret: process.env.AUTH0_CLIENTSECRET,
		scope: process.env.AUTH0_SCOPE,
		jwtSettings: {
			secret: jwks.expressJwtSecret({
				cache: true,
				rateLimit: true,
				jwksRequestsPerMinute: 5,
				jwksUri: process.env.AUTH0_JWKSURI
			}),
			audience: process.env.AUTH0_AUDIENCE,
      issuer: `https://${process.env.AUTH0_DOMAIN}/`,
			algorithms: ['RS256']
		}
	},

	db: {
		host: process.env.DB_HOST || 'localhost',
		port: Number(process.env.DB_PORT) || 5432,
		dialect: process.env.DB_DIALECT || 'postgres',
		username: process.env.DB_USERNAME || 'username',
		password: process.env.DB_PASSWORD || 'password',
		database: process.env.DB_DATABASE || 'database',
    sync: (process.env.DB_SYNC === 'true') || false
	},

	logger: {
		level: process.env.LOGGER_LEVEL || 'silly',
		host: process.env.LOGGER_HOST,
		port: Number(process.env.LOGGER_PORT),
	},

	sentry: {
		dsn: process.env.SENTRY_DSN,
	},

	email: {
		backend: 'sendgrid',
		sender: {
			default: {
				name: process.env.EMAIL_SENDER_DEFAULT_NAME,
				email: process.env.EMAIL_SENDER_DEFAULT_EMAIL,
			},
			support: {
				name: process.env.EMAIL_SENDER_SUPPORT_NAME,
				email: process.env.EMAIL_SENDER_SUPPORT_EMAIL,
			},
		},
		sendgrid: {
			secret: process.env.EMAIL_SENDGRID_SECRET,
			template: process.env.EMAIL_SENDGRID_TEMPLATE
		}
	},

  recaptcha: {
    key     : process.env.RECAPTCHA_KEY,
    secret  : process.env.RECAPTCHA_SECRET
  },

  admin: {
    name  : process.env.ADMIN_NAME || 'admin',
    email : process.env.ADMIN_MAIL || 'noreply@example.com'
  }
}

export { config }
