import * as express from 'express';
import { getRepository } from 'typeorm';
import Controller from './../../interfaces/controller.interface';
import Farm from './../../controllers/farm/farm.entity';
import User from './../../controllers/user/user.entity';
import Stream from './../../controllers/stream/stream.entity';
import DataType from './../../controllers/datatype/datatype.entity';
import DataTypeUnit from './../../controllers/datatype/datatype_unit.entity';
import Datapoint from './../../controllers/datapoint/datapoint.entity';
import CreateStreamDto from './../../controllers/stream/stream.dto';
import logger from './../../utils/logger';

class FarmController implements Controller {

  public path = '/stream';
  public router = express.Router();

  constructor() {
    this.initializeRoutes();
  }

  // Setup routes for this controller
  private initializeRoutes() {
    this.router.get(`${this.path}`, this.index);
    this.router.get(`${this.path}/:id`, this.show);
    this.router.get(`${this.path}/:id/data`, this.fetchData);

    this.router.post(`${this.path}`, this.create);
    this.router.put(`${this.path}/:id`, this.update);
    this.router.delete(`${this.path}/:id`, this.destroy);
  }

  /**
   * Return a list of streams
   **/
  private index = async (req: express.Request, res: express.Response, _next: express.NextFunction) => {

    const sub: string = req.user.sub;
    logger.info('[ getCurrentUser ] - sub ' + sub);

    const farms: Farm[] = await getRepository(Farm).find();
    res.send(farms);
  }

  /**
   * Show the details of a stream
   **/
  private show = async (req: express.Request, res: express.Response, _next: express.NextFunction) => {

    let stream: Stream;
    const streamId: number = req.params.id;

    try {
      stream = await getRepository(Stream).findOne(streamId, {
        relations: [ 'farm' ]
      });
    } catch (err) {
      logger.warn(err);
      return res.status(500).json(err);
    }

    res.json(stream);
  }

  /**
   * Fetch and return datapoints for a specific stream
   **/
  private fetchData = async (req: express.Request, res: express.Response, _next: express.NextFunction) => {
    console.log(req.params);
    const streamId: number = req.params.id;
    logger.info(`[ stream.controller ] - fetchData for ${streamId}`)
    let datapoints: Datapoint[];

    try {
      datapoints = await getRepository(Datapoint).find({
        where: {
          stream: streamId
        },
        take: 50
      });
    } catch (err) {
      logger.warn(err);
      return res.status(500).json(err);
    }

    res.json(datapoints);
  }

  /**
   * Create a new stream
   **/
  private create = async (req: express.Request, res: express.Response, _next: express.NextFunction) => {
    logger.info('[ farm.controller - createStream ] - Init. ');
    let user: User;
    let farm: Farm;
    let stream: Stream;
    let dataType: DataType;
    let dataTypeUnit: DataTypeUnit;

    try {
      const sub: string = req.user.sub;
      logger.info('[ farm.controller - createStream ] - getCurrentUser: ' + sub);
      user = await getRepository(User).findOne({ where: { auth0: sub }});

      if (!user || !user.id) {
        logger.info('[ farm.controller - createStream ] - User not found. ');
        return res.status(403).json({ msg: 'User not found' });
      }

      const farmId: string = req.body.farmId;
      logger.info('[ farm.controller - createStream ] - getFarm: ' + farmId);
      farm = await getRepository(Farm).findOne(farmId);

      if (!farm || !farm.id) {
        logger.info('[ farm.controller - createStream ] - Farm not found. ');
        return res.status(403).json({ msg: 'Farm not found' });
      }

      dataType = await getRepository(DataType).findOne(parseInt(req.body.datatype_id));
      if (!dataType || !dataType.id) {
        logger.info('[ farm.controller - createStream ] - DataType not found. ');
        return res.status(403).json({ msg: 'DataType not found' });
      }

      dataTypeUnit = await getRepository(DataTypeUnit).findOne(parseInt(req.body.datatype_unit_id));
      if (!dataTypeUnit || !dataTypeUnit.id) {
        logger.info('[ farm.controller - createStream ] - dataTypeUnit not found. ');
        return res.status(403).json({ msg: 'dataTypeUnit not found' });
      }
    } catch(err) {
      console.log(err);
      return res.status(500).json(err);
    }

    // Validate permission for farm with this user

    // Create the stream
    logger.info('[ farm.controller - createStream ] - creating a new stream for farm ' + farm.id, req.body);
    // Create the new entity
    const streamData = new CreateStreamDto();
          streamData.name = req.body.name;
          streamData.description = req.body.description;
          streamData.farm = farm;
          streamData.datatype = dataType;
          streamData.datatypeUnit = dataTypeUnit;
    console.log('streamData', streamData);

    try {
      stream = await getRepository(Stream).create({ ...streamData });
      logger.info('[ farm.controller - createStream ] - Stream created. ');
      await getRepository(Stream).save(stream);
    } catch (err) {
      logger.warn(err);
      return res.status(500).json(err);
    }
    logger.info('[ farm.controller - createStream ] - Stream saved. ');
    res.send(farm);
  }

  /**
   * Update an existing stream
   **/
  private update = async (req: express.Request, res: express.Response, _next: express.NextFunction) => {
    const sub: string = req.user.sub;
    logger.info('[ getCurrentUser ] - sub ' + sub);

    const farms: Farm[] = await getRepository(Farm).find();
    res.send(farms);
  }

  /**
   * Delete an existing stream
   **/
  private destroy = async (req: express.Request, res: express.Response, _next: express.NextFunction) => {
    const sub: string = req.user.sub;
    logger.info('[ getCurrentUser ] - sub ' + sub);

    const farms: Farm[] = await getRepository(Farm).find();
    res.send(farms);
  }
}

export default FarmController;
