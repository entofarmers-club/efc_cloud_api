import {
  IsNumber,
  IsString,
  Length,
  ValidateNested } from 'class-validator';

class FarmInStreamDto {
  @IsNumber()
  id: number;
}
class DataTypeInStreamDto {
  @IsNumber()
  id: number;
}
class DataTypeUnitInStreamDto {
  @IsNumber()
  id: number;
}

class CreateStreamDto {

  @Length(3, 50)
    name: string;

  @IsString()
    description: string;

  @ValidateNested()
    farm: FarmInStreamDto

  @ValidateNested()
    datatype: DataTypeInStreamDto;

  @ValidateNested()
    datatypeUnit: DataTypeUnitInStreamDto;

}

export default CreateStreamDto;
