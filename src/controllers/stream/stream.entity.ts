import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn } from 'typeorm';
import {
  IsNumber,
  IsString,
  Length } from 'class-validator';
import Farm from './../../controllers/farm/farm.entity';
import Datatype from './../../controllers/datatype/datatype.entity';
import DatatypeUnit from './../../controllers/datatype/datatype_unit.entity';
import Datapoint from './../../controllers/datapoint/datapoint.entity';

@Entity()
class Stream {

  @PrimaryGeneratedColumn()
    readonly id: number;

  @Column()
  @Length(3, 50)
    name: string;

  @Column({ type: 'text' })
  @IsString()
    description: string;

  @Column({ nullable: true })
  @IsNumber()
    currentValue: number;

  @CreateDateColumn({
      name: 'created_at',
      type: 'timestamptz'
    })
    createdAt: Date

  @UpdateDateColumn({
      name: 'updated_at',
      type: 'timestamptz'
    })
    updatedAt: Date

  @ManyToOne(_type => Farm, farm => farm.streams)
    @JoinColumn({ name: 'farm_id' })
    farm: Farm;

  @ManyToOne(() => Datatype, {
        eager: true
    })
    @JoinColumn({ name: 'datatype_id' })
    datatype: Datatype;

  @ManyToOne(() => DatatypeUnit, {
        eager: true
    })
    @JoinColumn({ name: 'datatype_unit_id' })
    datatypeUnit: DatatypeUnit;

  @OneToMany(_type => Datapoint, datapoint => datapoint.stream)
    datapoints: Datapoint[];
}

export default Stream;
