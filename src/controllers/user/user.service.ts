import { getRepository } from 'typeorm';
import User from './../../controllers/user/user.entity';
import CreateUserDto from './../../controllers/user/user.dto';
import logger from './../../utils/logger';

// Create new user in the local db
export async function CreateUser(userData: CreateUserDto): Promise<User> {

  try {
    let user: User = await getRepository(User).create({ ...userData});
    await getRepository(User).save(user);
    return user;
  } catch(err) {
    logger.warn(err);
    return;
  }
}
