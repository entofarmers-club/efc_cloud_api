import {
  IsBoolean,
  IsEmail,
  IsString,
  Length } from 'class-validator';

class CreateUserDto {
  @IsString()
  public auth0: string;

  @IsEmail()
  public email: string

  @IsString()
  @Length(3,25)
  public username: string

  @IsString()
  public picture: string

  @IsString()
  public plan: string

  @IsString()
  public type: string

  @IsBoolean()
  public admin?: boolean

}

export default CreateUserDto;
