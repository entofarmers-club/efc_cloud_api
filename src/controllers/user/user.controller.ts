import * as express from 'express';
import { getRepository, Like, Not } from 'typeorm';
import Controller from './../../interfaces/controller.interface';
// import PostNotFoundException from './../exceptions/PostNotFoundException';
import CreateUserDto from './../../controllers/user/user.dto';
import User from './../../controllers/user/user.entity';
import logger from './../../utils/logger';
import { getUser } from './../../services/auth0.service';

class UserController implements Controller {

  public path = '/user';
  public router = express.Router();
  private userRepository = getRepository(User);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/me`, this.getCurrentUser);
    this.router.get(`${this.path}/search`, this.searchUser);
  }

  /**
   *
   * ------------------
   * APP STARTING POINT
   * ------------------
   * Current user balance and latest transactions.
   * Create a new user if user does not yet exists locally.
   *
   **/
  private getCurrentUser = async (req: express.Request, res: express.Response, _next: express.NextFunction) => {

    const sub: string = req.user.sub;
    logger.info('[ getCurrentUser ] - sub ' + sub);

    let user: User;

    // Fetch user from app-db
    try {
      user = await getRepository(User).findOne({auth0: sub});
    } catch(err) {
      logger.warn(err);
      return res.status(500).json(err);
    }
    logger.debug({message: '[ getCurrentUser ] - Local user fetched ', user });

    // Create a new user if not existing and fetch profile from auth0
    if (!user) {
      logger.info({ message: 'No user exists locally, creating one. ', file: 'user.controller' });
      try {
        const auth0Profile:any = await getUser(sub);
        if (!auth0Profile) {
          logger.warn('[ getCurrentUser ] - requested profile not found: ' + sub);
          return res.status(500).json('requested profile not found: ' + sub);
        }
        logger.debug({message: '[ getCurrentUser ] - Profile fetched ', auth0Profile });

        const userData: CreateUserDto = {
          auth0: sub,
          email: auth0Profile.email,
          username: auth0Profile.username,
          picture: auth0Profile.picture,
          type: 'personal',
          plan: 'alpha'
        };
        user = await this.userRepository.create({ ...userData });

        await this.userRepository.save(user);

      } catch(err) {
        logger.warn(err);
        return res.status(500).json(err);
      }
    }

    res.send(user);
  }

  //
  private searchUser = async (req: express.Request, res: express.Response, _next: express.NextFunction) => {

    logger.info('[ account.controller search ] - Query for: ', req.query);

    // Validation
    if (!req.query.username) { return res.status(403).json({error: 'No username provided'}); }
    let username = req.query.username.toString();
    if (username.length<3) { return res.json(null); }

    //
    const users: User[] = await getRepository(User).find({
      where: {
        username: Like(`%${username}%`),
        auth0: Not(req.user.sub)
      },
      select: ['username', 'picture', 'auth0'],
      take:20
    });

    return res.json(users);
  }
}

export default UserController;
