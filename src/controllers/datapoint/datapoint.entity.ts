import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn } from 'typeorm';
import {
  IsDate,
  IsNumber } from 'class-validator';
import Stream from './../../controllers/stream/stream.entity';

@Entity()
class Datapoint {

  @PrimaryGeneratedColumn()
    readonly id: number;

  @Column({ type: 'timestamptz' })
  @IsDate()
    timestamp: Date;

  @Column()
  @IsNumber()
    value: number;

  @ManyToOne(() => Stream)
    @JoinColumn({ name: 'stream_id' })
    stream: Stream;

}

export default Datapoint;
