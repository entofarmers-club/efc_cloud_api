import { getRepository } from 'typeorm';
import Datapoint from './../../controllers/datapoint/datapoint.entity';
import Stream from './../../controllers/stream/stream.entity';
import CreateDatapointDto from './../../controllers/datapoint/datapoint.dto';
import logger from './../../utils/logger';

class DatapointService {
  // Add a datapoint to the datastore
  async addDatapoint(params:any) {

    return new Promise(async (resolve, reject) => {

      let a = parseFloat(params.value);

      if (isNaN(a)) { return reject('not a valid number'); }

      let datapoint: Datapoint;
      let datapointData: CreateDatapointDto;
      let stream: Stream;

      stream = await getRepository(Stream).findOne(params.streamId);

      datapointData = new CreateDatapointDto();
      datapointData.value = a;
      datapointData.timestamp = new Date();
      datapointData.stream = stream;

      try {
        datapoint = await getRepository(Datapoint).create({ ...datapointData });
        logger.info('[ datapoint.service - createDatapoint ] - Datapoint created. ');
        await getRepository(Datapoint).save(datapoint);
      } catch (err) {
        logger.warn(err);
        return reject(err);
      }
      logger.info('[ datapoint.service - createDatapoint ] - Datapoint saved. ');
      resolve(datapoint);
    });
  }
}

export default new DatapointService();
