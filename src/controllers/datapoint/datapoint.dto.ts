import {
  IsDate,
  IsNumber,
  ValidateNested } from 'class-validator';

class StreamInDatapointDto {
  @IsNumber()
  id: number;
}

class CreateDatapointDto {

  @IsNumber()
    value: number;

  @IsDate()
    timestamp: Date

  @ValidateNested()
    stream: StreamInDatapointDto
}

export default CreateDatapointDto;
