import {
  EntitySubscriberInterface,
  EventSubscriber,
  getRepository,
  InsertEvent } from 'typeorm';
import Datapoint from './datapoint.entity';
import Stream from './../stream/stream.entity';
import logger from './../../utils/logger';

@EventSubscriber()
export class DatapointSubscriber implements EntitySubscriberInterface<Datapoint> {

  listenTo() { return Datapoint }

  // Update current value for the stream of the datapoint
  async updateCurrentValue(entity: Datapoint): Promise<boolean> {
    logger.info(`[ datapoint.subscriber ] - updateCurrentValue. `);
    try {
      let stream: Stream = await getRepository(Stream).findOne(entity.stream.id);
      stream.currentValue = entity.value;
      await getRepository(Stream).save(stream);
    } catch(err) {
      logger.warn(`[ datapoint.subscriber ] - updateCurrentValue error `, err);
      return false;
    }
    logger.info(`[ datapoint.subscriber ] - updateCurrentValue done. `);
    return true;
  }

  // Handle after inserting a new datapoint
  async afterInsert(event: InsertEvent<Datapoint>) {
    const result = await this.updateCurrentValue(event.entity);
    logger.info(`[ datapoint.subscriber ] - afterInsert status: ${result}`);
  }
}
