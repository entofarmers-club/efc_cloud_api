import { Entity, Column, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Length, IsString } from 'class-validator';
import Datatype from './../../controllers/datatype/datatype.entity';

@Entity()
class DatatypeUnit {

  @PrimaryGeneratedColumn()
    readonly id: number;

  @Column({ nullable: false })
  @IsString()
  @Length(1, 10)
    unit: string;

  @Column({ nullable: false })
  @IsString()
  @Length(1,10)
    constant: string;

  @Column({ nullable: true })
  @IsString()
  @Length(1,10)
    value: string;

  @ManyToMany(() => Datatype, datatype => datatype.units)
    datatypes: Datatype[];

}

export default DatatypeUnit;
