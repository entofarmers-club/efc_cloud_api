import * as express from 'express';
import { getRepository } from 'typeorm';
import Controller from './../../interfaces/controller.interface';
import Datatype from './../../controllers/datatype/datatype.entity';
import logger from './../../utils/logger';

class DatatypeController implements Controller {

  public path = '/datatype';
  public router = express.Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/`, this.getDatatypes);
  }

  // Fetch datatypes
  async getDatatypes(
    _req: express.Request,
    res: express.Response,
    _next: express.NextFunction) {

    logger.info('[ datatypes.controller ] - fetch types');
    let dt: Datatype[];

    try {
      dt = await getRepository(Datatype).find({
        relations: ['units']
      });
    } catch(err) {
      logger.warn(err);
      return res.status(500).json(err);
    }

    res.json(dt);
  }
}

export default DatatypeController;
