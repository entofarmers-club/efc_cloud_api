
export const DatatypeSeed = [
  {
    type: "Analog Actuator",
    constant: "ANALOG_ACTUATOR",
    value: "analog_actuator",
    units: [ 'ANALOG' ]
  },
  {
    type: "Digital Actuator",
    constant: "DIGITAL_ACTUATOR",
    value: "digital_actuator",
    units: [ 'DIGITAL' ]
  },
  {
    type: "Digital Sensor",
    constant: "DIGITAL_SENSOR",
    value: "digital_sensor",
    units: [ 'DIGITAL' ]
  },
  {
    type: "Analog Sensor",
    constant: "ANALOG_SENSOR",
    value: "analog_sensor",
    units: [ 'ANALOG' ]
  },
  {
    type: "Acceleration",
    constant: "ACCELERATION",
    value: "accel",
    units: [ 'G' ]
  },
  {
    type: "Bandwidth",
    constant: "BANDWIDTH",
    value: "bw",
    units: [ 'GBPS', 'GBPS', 'MBPS' ]
  },
  {
    type: "Barometric pressure",
    constant: "BAROMETRIC_PRESSURE",
    value: "bp",
    units: ['PASCAL', 'HECTOPASCAL']
  },
  {
    type: "Battery",
    constant: "BATTERY",
    value: "batt",
    units: ['PERCENT', 'RATIO','VOLTS']
  },
  {
    type: "Carbon Monoxide",
    constant: "CO",
    value: "co",
    units: ['PPM']
  },
  {
    type: "Carbon Dioxide",
    constant: "CO2",
    value: "co2",
    units: ['PPM']
  },
  {
    type: "CPU Load",
    constant: "CPU_LOAD",
    value: "cpuload",
    units: ['PERCENT']
  },
  {
    type: "Counter",
    constant: "COUNTER",
    value: "counter",
    units: ['ANALOG']
  },
  {
    type: "Current",
    constant: "CURRENT",
    value: "current",
    units: ['AMP','MAMP']
  },
  {
    type: "Energy",
    constant: "ENERGY",
    value: "energy",
    units: ['KWH']
  },
  {
    type: "External Waterleak",
    constant: "EXT_WATERLEAK",
    value: "ext_wleak",
    units: ['ANALOG']
  },
  {
    type: "Frequency",
    constant: "FREQUENCY",
    value: "freq",
    units: ['HERTZ']
  },
  {
    type: "GPS",
    constant: "GPS",
    value: "gps",
    units: ['GPS']
  },
  {
    type: "Temperature",
    constant: "TEMPERATURE",
    value: "temp",
    units: ['FAHRENHEIT', 'CELSIUS', 'KELVIN']
  },
  {
    type: "Voltage",
    constant: "VOLTAGE",
    value: "voltage",
    units: ['VOLTS', 'MILLIVOLTS']
  },
  {
    type: "Trap",
    constant: "TRAP",
    value: "trap",
    units: ['DIGITAL']
  },
  {
    type: "Waterleak",
    constant: "WATERLEAK",
    value: "waterleak",
    units: ['DIGITAL']
  },
  {
    type: "Wind Speed",
    constant: "WIND_SPEED",
    value: "wind_speed",
    units: ['KM_PER_H']
  }
];

export const DatatypeUnitSeed = [
  {
    unit: 'Analog',
    constant: 'ANALOG'
  },
  {
    unit: 'Digital (0/1)',
    constant: 'DIGITAL',
    value: 'd'
  },
  {
    unit: 'Acceleration',
    constant: 'G',
    value: 'g',
    map: 'gx,gy,gz'
  },
  {
    unit: 'Kbps',
    constant: 'KBPS',
    value: 'kbps'
  },
  {
    unit: 'Gbps',
    constant: 'GBPS',
    value: 'gbps'
  },
  {
    unit: 'Mbps',
    constant: 'MBPS',
    value: 'mbps'
  },
  {
    unit: 'Pascal',
    constant: 'PASCAL',
    value: 'pa'
  },
  {
    unit: 'Hecto Pascal',
    constant: 'HECTOPASCAL',
    value: 'hpa'
  },
  {
    unit: '% (0 to 100)',
    constant: 'PERCENT',
    value: 'p'
  }, {
    unit: 'Ratio',
    constant: 'RATIO',
    value: 'r'
  }, {
    unit: 'Volts',
    constant: 'VOLTS',
    value: 'v'
  },
  {
    unit: 'Millivolts',
    constant: 'MILLIVOLTS',
    value: 'mv',

  },
  {
    unit: 'Parts per milliion',
    constant: 'PPM',
    value: 'ppm'
  },
  {
    unit: 'Ampere',
    constant: 'AMP',
    value: 'a',
  },
  {
    unit: 'Milliampere',
    constant: 'MAMP',
    value: 'ma'
  },
  {
    unit: 'Kilometer per hour',
    constant: 'KM_PER_H',
    value: 'kmh'
  },
  {
    unit: 'Fahrenheit',
    constant: 'FAHRENHEIT',
    value: 'f',
  },
  {
    unit: 'Celsius',
    constant: 'CELSIUS',
    value: 'c',
  },
  {
    unit: 'Kelvin',
    constant: 'KELVIN',
    value: 'k',
  },
  {
    unit: 'Killowatt Hour',
    constant: 'KWH',
    value: 'kwh',
  },
  {
    unit: 'Hertz',
    constant: 'HERTZ',
    value: 'hz',
  },
  {
    unit: 'GPS',
    constant: 'GPS',
    value: 'm',
    map: 'lat,long,alt'
  }
];
