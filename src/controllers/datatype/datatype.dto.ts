import { IsString } from 'class-validator';

class CreateDatatypeDto {

  @IsString()
    type: string;

  @IsString()
    constant: string;

  @IsString()
    value: string;

}

export default CreateDatatypeDto;
