import {
  Entity,
  Column,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn } from 'typeorm';
import {
  Length,
  IsString } from 'class-validator';
import DatatypeUnit from './../../controllers/datatype/datatype_unit.entity';

@Entity()
class Datatype {

  @PrimaryGeneratedColumn()
    readonly id: number;

  @Column({ nullable: false })
  @IsString()
  @Length(1, 10)
    type: string;

  @Column({ nullable: false })
  @IsString()
  @Length(1,10)
    constant: string;

  @Column({ nullable: false })
  @IsString()
  @Length(1,10)
    value: string;

  @ManyToMany(() => DatatypeUnit)
    @JoinTable()
    units: DatatypeUnit[];

}

export default Datatype;
