import * as express from 'express';
import { getRepository } from 'typeorm';
import Controller from './../../interfaces/controller.interface';
// import PostNotFoundException from './../exceptions/PostNotFoundException';
import CreateFarmDto from './../../controllers/farm/farm.dto';
import Farm from './../../controllers/farm/farm.entity';
import User from './../../controllers/user/user.entity';
import logger from './../../utils/logger';

class FarmController implements Controller {

  public path = '/farm';
  public router = express.Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.index);
    this.router.get(`${this.path}/:id`, this.show);
    this.router.post(`${this.path}`, this.create);
    this.router.put(`${this.path}/:id`, this.update);
    this.router.delete(`${this.path}/:id`, this.destroy);
  }

  //
  private index = async (req: express.Request, res: express.Response, _next: express.NextFunction) => {

    const sub: string = req.user.sub;
    logger.info('[ getCurrentUser ] - sub ' + sub);

    const farms: Farm[] = await getRepository(Farm).find();
    res.json(farms);
  }

  //
  private show = async (req: express.Request, res: express.Response, _next: express.NextFunction) => {
    logger.info('[ farm.controller - showFarm ] - Init. ');

    let farm: Farm;

    try {
      const id = parseInt(req.params.id);
      farm = await getRepository(Farm).findOne(id);
    } catch (err) {
      return res.status(500).json(err);
    }

    res.json(farm);
  }

  /**
   *
   * Create a new farm
   *
   **/
  private create = async (req: express.Request, res: express.Response, _next: express.NextFunction) => {
    logger.info('[ farm.controller - createFarm ] - Init. ');
    let user: User;
    let farm: Farm;

    const sub: string = req.user.sub;
    logger.info('[ farm.controller - createFarm ] - getCurrentUser: ' + sub);
    // console.log(getRepository(User));
    user = await getRepository(User).findOne({ where: { auth0: sub }});
    console.log(user);
    if (!user || !user.id) {
      logger.info('[ farm.controller - createFarm ] - User not found. ');
      return res.status(403).json({ msg: 'User not found' });
    }

    logger.info('[ farm.controller - createFarm ] - creating a new farm for user ' + user.id);
    // Create the new entity
    const farmData = new CreateFarmDto();
          farmData.name = req.body.name;
          farmData.description = req.body.description;
          farmData.user = user;

    try {
      farm = await getRepository(Farm).create({ ...farmData });
      logger.info('[ farm.controller - createFarm ] - Farm created. ');
      await getRepository(Farm).save(farm);
    } catch (err) {
      logger.warn(err);
      return res.status(500).json(err);
    }
    logger.info('[ farm.controller - createFarm ] - Farm saved. ');
    res.send(farm);
  }

  //
  private update = async (req: express.Request, res: express.Response, _next: express.NextFunction) => {
    logger.info('[ farm.controller - updateFarm ] - Init. ');
    let user: User;
    let farm: Farm;

    const sub: string = req.user.sub;
    logger.info('[ farm.controller - updateFarm ] - getCurrentUser: ' + sub);
    // console.log(getRepository(User));
    user = await getRepository(User).findOne({ where: { auth0: sub }});
    if (!user || !user.id) {
      logger.info('[ farm.controller - updateFarm ] - User not found. ');
      return res.status(403).json({ msg: 'User not found' });
    }

    try {

      farm = await getRepository(Farm).findOne(req.params.id);
      if (req.body.name) { farm.name = req.body.name; }
      if (req.body.description) { farm.description = req.body.description; }
      await getRepository(Farm).save(farm)
    } catch (err) {
      logger.warn(err);
      return res.status(500).json(err);
    }
    res.send(farm);
  }

  //
  private destroy = async (req: express.Request, res: express.Response, _next: express.NextFunction) => {
    const sub: string = req.user.sub;
    logger.info('[ getCurrentUser ] - sub ' + sub);

    const farms: Farm[] = await getRepository(Farm).find();
    res.send(farms);
  }
}

export default FarmController;
