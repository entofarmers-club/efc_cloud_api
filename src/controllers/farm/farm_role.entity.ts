import {
  Entity,
  Column,
  ManyToOne } from 'typeorm';
import { IsString } from 'class-validator';
import User from './../../controllers/user/user.entity';
import Farm from './../../controllers/farm/farm.entity';

@Entity()
export class FarmRoleUser {

  @Column()
  @IsString()
    role: string; // enum

  @ManyToOne(() => User, user => user.farms, { primary: true })
    user: User;

  @ManyToOne(() => Farm, farm => farm.users, { primary: true })
    farm: Farm;
}
