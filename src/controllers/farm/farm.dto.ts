import {
  IsNumber,
  IsString,
  Length,
  ValidateNested } from 'class-validator';

class UserInFarmDto {
  @IsNumber()
  id: number;
}

class CreateFarmDto {
  @IsString()
  @Length(3,50)
    name: string;

  @IsString()
    description?: string

  @ValidateNested()
    user: UserInFarmDto
}

export default CreateFarmDto;
