import uuidv4 from 'uuid/v4';
import {
  BeforeInsert,
  Entity,
  Column,
  OneToMany,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn } from 'typeorm';
import {
  IsBoolean,
  Length,
  IsString } from 'class-validator';
import User from './../../controllers/user/user.entity';
import Stream from './../../controllers/stream/stream.entity';

@Entity()
class Farm {

  @PrimaryGeneratedColumn()
    readonly id: number;

  @Column()
  @Length(3, 50)
    name: string;

  @Column({ type: 'text', nullable: true })
  @IsString()
    description: string;

  @Column({ default: false })
  @IsBoolean()
    mqtt_status: boolean

  @Column({ nullable: true })
  @IsString()
    mqtt_username: string;

  @Column({ nullable: true })
  @IsString()
    mqtt_password: string

  @Column({ nullable: true })
  @IsString()
    mqtt_client: string

  @CreateDateColumn({
      name: 'created_at',
      type: 'timestamptz'
    })
    createdAt: Date

  @UpdateDateColumn({
      name: 'updated_at',
      type: 'timestamptz'
    })
    updatedAt: Date


  @OneToMany(() => Farm, farm => farm.users)
    users: User[];

  @OneToMany(_type => Stream, stream => stream.farm, {
      eager: true
    })
    streams: Stream[];


  @BeforeInsert()
  createuuid() {
    console.log(`[ farm.entity ] - BeforeInsert: add mqtt credentials.`);
    this.mqtt_username = uuidv4();
    this.mqtt_password = uuidv4();
    this.mqtt_client = uuidv4();
  }
}

export default Farm;
