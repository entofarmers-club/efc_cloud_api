module.exports = {

  apps : [{
    name: 'entofarmers_api',
    script: 'npm -- start',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',

    env: {

      NODE_ENV: 'development',
      NODE_PORT: 4000,
      MQTT_PORT: 1883,
      BASE_URL: 'http://localhost:4000',

      PRODUCT_URL: 'http://localhost:4200',
      AUTH_URL: 'http://localhost:4100',
      API_URL: 'http://localhost:4000',

      DB_DATABASE: 'oif',
      DB_USERNAME: 'postgres',
      DB_PASSWORD: 'password',
      DB_HOST: 'localhost',
      DB_DIALECT: 'postgres',
      DB_SYNC: true,
      DB_SYNC_FORCE: true,

      AUTH0_NAMESPACE: "https://entofarmers.club/api",
      AUTH0_ISSUER: "https://oif.eu.auth0.com/",
      AUTH0_AUDIENCE: "https://entofarmers.club/api",
      AUTH0_JWKSURI: "https://oif.eu.auth0.com/.well-known/jwks.json",

      LOGGER_LEVEL: 'silly',

    },
    env_production: {
      NODE_ENV: 'production'
    }
  }]
};
